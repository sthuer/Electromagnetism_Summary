\section{Magnetostatics}
\subsection{Lorentz law}
\label{Lorentz law}
If a charge $q$ is moving with velocity $\vec{v}$ in a magnetic field $\vec{B}$,
then a force (Lorentz force) acts perpendicular to the direction of the charge and the magnetic field.
\begin{equation}\label{lorentz_law}
    \vec{F}_M = q ( \vec{v} \times \vec{B} )
\end{equation}
Hence the total force on the charge $q$ is given by adding the lorentz force and the electric force.
\begin{equation}\label{force_on_moving_charge}
    \vec{F} = q \left(  \vec{E} + (\vec{v}\times\vec{B})  \right)
\end{equation}
Because the Lorentz force is always perpendicular to the direction of travel,
no work is done.


\subsubsection{Cyclotron}
\label{Cyclotron}
Assuming a moving charge $q$ is moving with a velocity $\vec{v}$ in a uniform magnetic field $\vec{B}$.
\begin{equation}\label{cyclotron_force}
    \vec{F}_B = q \vec{v} \times \vec{B} = -qvB_0 \hat{r}
\end{equation}
Which is the centripetal force known from General Physics I: Mechanics.
\begin{equation*}
    \vec{a} = -\frac{q v B_0}{m}\hat{r} = -\frac{v^2}{r}\hat{r}
\end{equation*}
And by extension the Cyclotron has a radius of
\begin{equation}\label{cyclotrone_frequency}
    \begin{split}
        r &= \frac{m v}{q B_0}
        \\
        \vec{\omega} &= -\frac{q}{m}\vec{B}
        \\
        \omega_c &= \frac{q}{m}B_0
    \end{split}
\end{equation}
$\omega_c$ is the Cyclotron frequency.


\subsubsection{Magnetic force of current in a wire}
\label{Magnetic force of current in a wire}
Basically just use eq \eqref{lorentz_law} and sum over all charges.
\begin{equation}
    \begin{split}
        \vec{f}_{mag} &= n q \vec{v} \times \vec{B}
        \\
        \vec{F}_{mag} &= \iiint_V \vec{j} \times \vec{B} \, dV
        \\
        &= \int_\Gamma \vec{j} \times\vec{B} S \,d\vec{l}
    \end{split}
\end{equation}
where $V$ is the volume of wire and $\Gamma$ is the path along the wire.
This can also be simplified if the current density $\vec{j}$ and its cross section don't change.
\begin{equation}\label{f_mag_in_a_wire}
    \begin{split}
        \vec{F}_{mag} &= I \int \vec{u}_t \times \vec{B}(x,y,z)\,dl
        \\
        \vec{F}_{mag} &= I \int ( d\vec{l} \times \vec{B} )
    \end{split}
\end{equation}
where $I$ is the technical current and $\vec{u}_t$ is the unit vector in the direction of the cable.


\subsection{Ampère-Laplace law}
\label{Ampère-Laplace law}
If currents are constant ($\nabla\cdot\vec{j}=0$),
then the current produces a static magnetic field (ergo magnetostatics).
\begin{equation}\label{B_field_ampere-laplace_along_wire}
    \vec{B}(\vec{x}) =
    \frac{\mu_0}{4\pi} \int_\Gamma
    \frac{\vec{I} d\vec{l}' \times  (\vec{x}-\vec{r}')}
    { \left\lVert \vec{x} - \vec{r}' \right\rVert  ^3 }
\end{equation}
or substituting $I$ for $\vec{j}dA$
\begin{equation}\label{B_field_ampere-laplace_over_volume}
    \vec{B}(\vec{x}) =
    \frac{\mu_0}{4\pi} \iiint_V
    \frac{\vec{j}(\vec{r}') \times  (\vec{x}-\vec{r}')}
    { \left\lVert \vec{x} - \vec{r}' \right\rVert  ^3 }
    \,dV
\end{equation}


\subsection{Divergence and curl of the magnetic field}
\label{Divergence and curl of the magnetic field}
Magnetic fields have no origin,
always circling back.
Ergo the divergence is always 0.
\begin{equation}\label{div_B}
    \nabla \cdot \vec{B} = 0
\end{equation}


\subsubsection{Amperian loop}
\label{Amperian loop}
Integrating around a curve gives the total current enclosed by it.
\begin{equation} \label{closed_path_integral_b_field_enclosed_current}
    \begin{split}
        \oint_\Gamma \vec{B} \cdot \,d\vec{l} \, & = 2\pi r\frac{\mu_0 I}{2\pi r}
        \\
        & = \mu_0I
    \end{split}
\end{equation}
And by using Stoke's theorem for the curl we get the differential form.
\begin{equation}\label{amperian_loop_differential_form}
    \begin{split}
        \oint \vec{B} \cdot \,d\vec{l} &= \iint_S \nabla \times \vec{B} \,d\vec{a}
        \\
        \nabla \times \vec{B} &= \mu_0 \vec{j}
    \end{split}
\end{equation}


\subsubsection{Closed current loop}
\label{Closed current loop}
The magnetic field inside a coil of $n$ curls and with length $l$ can easily be found using an amperian loop.
\begin{equation}\label{coil/solenoid}
    B = \frac{n \mu_0 I}{l}
\end{equation}
In a uniform magnetic field $\vec{B}$,
the lorentz force $\vec{F}_L$ must be zero.
However, there may be a torque.
By defining the closed loop current as a dipole moment $\vec{M}$
\begin{equation}\label{magnetic_moment}
    \vec{M} = I\vec{S}
\end{equation}
the torque can then be written such that the magnetic moment tries to align itself with the magnetic field.
\begin{equation}\label{torque_on_magnetic_moment}
    \vec{\tau} = I\vec{S} \times \vec{B} = \vec{M}\times \vec{B}
\end{equation}
Hence the energy of the state (its rotation relative to the permanent field $\vec{b}$) is given by the dot product.
\begin{equation}\label{energy_of_magnetic_moment_in_B}
    E_{mag} = - \vec{M} \cdot \vec{B} = - \left\lVert \vec{M}\right\rVert \left\lVert \vec{B}\right\rVert \cos\theta
\end{equation}


\subsection{Vector potential for magnetic field}
\label{Vector potential for magnetic field}
Because the divergence of $\vec{B}$ is 0,
we can define a vector potential for it.
\begin{equation}\label{vector_potential}
    \vec{B} = \nabla \times \vec{A}
\end{equation}
Which gives us the kinda useful equation relating the current density to the potential,
where the direction of the potential is the same as that of the current.
This is known as the poisson equation,
similar to the electrostatic one described by eq \eqref{poisson_equation}.
\begin{equation}\label{magnetic_poisson_equation}
    \nabla^2\vec{A} = \mu_0\vec{j}
\end{equation}
Or in its integral form.
\begin{equation}\label{mag_potential_from_current}
    \vec{A}(\vec{x}) = \frac{\mu_0}{4\pi}\iiint_V\frac{\vec{j}(\vec{x}')}{\left\lVert \vec{x}-\vec{x}' \right\rVert}\,dV
\end{equation}



% ================================================================================
% Not lecture notes, just additional stuff from griffiths
% ================================================================================



\subsection{additional :: The field of a magnetized object}
\label{The field of a magnetized object}
The rest of this section was not covered during the lectures,
but includes some noteworthy additions.


\subsubsection{additional :: magnetic dipole/moment}
\label{additional :: magnetic dipole/moment}
Although already briefly touched on in \autoref{Closed current loop},
I want to draw the parallels to \autoref{Bound charges} by relating the potential to the dipole.
Anyway, similar to eq \eqref{potential_of_dipole_material} we examine the potential of a dipole.
\begin{equation}\label{potential_of_magnetic_dipole}
    \vec{A}(\vec{r})
    = \frac{\mu_0}{4\pi} \iiint_V
    \frac{
        \vec{M} \times \left( \vec{r}-\vec{r}' \right)
    }{
        \left\lVert  \vec{r}-\vec{r}'  \right\rVert^2
    }\,dV
\end{equation}
By then developing this equation like in eq \eqref{potential_of_dielectric} we get the magnetic version of it.
\begin{equation}
    \vec{A}(\vec{r})
    = \frac{\mu_0}{4\pi}
    \iiint_V \frac{\nabla' \times \vec{M}(\vec{r}')}{\left\lVert  \vec{r}-\vec{r}'  \right\rVert}
    \, dV
    +
    \frac{\mu_0}{4\pi}
    \oiint_S \frac{\vec{M}(\vec{r}')\times \, d\vec{a}}{\left\lVert  \vec{r}-\vec{r}'  \right\rVert}
\end{equation}
and thus by using the surface current
\begin{equation}\label{surface_current}
    \vec{j}_b = \nabla \times \vec{M}
\end{equation}
and volume current
\begin{equation}\label{volume_current}
    \vec{K}_b = \vec{M} \times \vec{n}
\end{equation}
we get the potential in terms of the bound currents.
\begin{equation}
    \vec{A}(\vec{r})
    = \frac{\mu_0}{4\pi}
    \iiint_V \frac{\vec{j}_b(\vec{r}')}{\left\lVert  \vec{r}-\vec{r}'  \right\rVert}
    \, dV
    +
    \frac{\mu_0}{4\pi}
    \oiint_S \frac{\vec{K}_b(\vec{r}')}{\left\lVert  \vec{r}-\vec{r}'  \right\rVert} \, d\vec{a}
\end{equation}


\subsubsection{additional :: Auxilliary field H}
\label{additional :: Auxilliary field H}
\begin{equation}\label{auxilliary_field_H}
    \begin{split}
        \vec{H} &= \frac{1}{\mu_0}\vec{B}-\vec{M}
        \\
        \nabla \times  \vec{H} &= \vec{j}_f
        \\
        \oint_\Gamma \vec{H}\cdot\,d\vec{l} &= I_{f_{enc}}
    \end{split}
\end{equation}
The magnetic moment $\vec{M}$ is sustained by $\vec{B}$ and so when $\vec{B}$ is removed, the moment disappears.
In linear mediums
\begin{equation}\label{M_from_H}
    \vec{M} = \chi_m\vec{H}
\end{equation}
Where $\chi_m$ is the magnetic susceptibility.
This leads us to the equation for the magnetic field in terms of the auxiliary field $\vec{H}$.
\begin{equation}\label{B_from_H}
    \vec{B} = \mu_0(\vec{H} + \vec{M}) = \mu_0 ( 1 + \chi_m ) \vec{H}
\end{equation}
