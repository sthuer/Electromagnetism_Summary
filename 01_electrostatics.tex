\section{Electrostatics}
\label{Electrostatics}
\subsection{Electric field}
\label{Electric field}
\subsubsection{Coulomb's law}
The force on a charge $q_1$ due to a charge $q_2$ is given by
\begin{equation}\label{CoulombForce}
    \vec{F} = \frac{1}{4\pi\varepsilon_0}
    \frac{q_1 q_2}{r^2}
    \hat{r}
\end{equation}
where $r$ is the distance between the charge,
$\hat{r} = \vec{r_2}-\vec{r_1}$ is the direction of the force and $\varepsilon_0=8.85\times10^{-12} \frac{C^2}{N \cdot m^2}$.
If several charges are present,
the superposition principle applies (vector addition of all applicable force vectors.)

\subsubsection{Electric field}
\label{Electric field-subsubsection}
By removing the test charge ($q_1$ in \ref{CoulombForce}) we obtain the electric field such that
\begin{equation} \label{F=qE}
    \vec{F} = q_1 \vec{E}
\end{equation}
Hence
\begin{equation}
    \vec{E}(\vec{r_2}) =
    \frac{\vec{F}}{q_1} = \frac{1}{4\pi\varepsilon_0}
    \frac{q_2}{|\vec{r_1} - \vec{r_2}|^2}
    \frac{\vec{r_1} - \vec{r_2}}{|\vec{r_1} - \vec{r_2}|}
\end{equation}

\subsubsection{Continuous charge distribution}
\label{Continuous charge distribution}
When using a charge density,
the resulting electric field is the integral over the relevant charge distribution.
\begin{equation}\label{E_field_from_charge_disribution}
    \vec{E}(\vec{r}) = \frac{1}{4\pi\varepsilon_0}
    \iiint_V
    \frac{\rho(\vec{r'}) ( \vec{r}-\vec{r'} ) }
    {|\vec{r}-\vec{r'}|^3} \,dV
\end{equation}




\subsection{Divergence and Curl of electrostatic fields}
\label{Divergence and Curl of electrostatic fields}
\subsubsection{Electric flux}
\label{Electric flux}
Converting $\vec{E}$ field vectors to field lines,
where the magnitude of the vectors is given by the density of lines,
we introduce the notion of the electric flux $\Phi_E$ which is defined as electric field (lines) per area.
\begin{equation}\label{electric_flux}
    \Phi_E = \iint_S \vec{E} \cdot dS
\end{equation}

\subsubsection{Gauss's law}
\label{Gauss's law}
When calculating the electric flux \eqref{electric_flux} over a closed surface,
any charge situated on the outside will not affect the flux,
as the field lines entering the surface must also exit.
Ergo only charges inside the surface have effect on the flux.
\begin{equation}\label{gaussian-surface_Qenc}
    \oiint_S \vec{E} \cdot dS = \frac{Q_{enc}}{\varepsilon_0}
\end{equation}
Using the divergence theorem and writing $Q_{enc}$ as a continuous charge distribution we get
\begin{equation}\label{div_E}
    \begin{split}
        \iiint_V (\nabla \cdot \vec{E}) dV & = \iiint_V \frac{\rho}{\varepsilon_0} dV
        \\
        \Rightarrow  \nabla \cdot \vec{E} & = \frac{\rho}{\varepsilon_0}
    \end{split}
\end{equation}

\subsubsection{Curl of E}
\label{Curl of E}
With the coulomb force being conservative,
(similar to gravity) the integral $\int_a^b \vec{E} \cdot dl$ is path independent.
Therefore
\begin{equation}\label{path_integral_in_E}
    \int_a^b \vec{E} dl
    = \frac{1}{4\pi\varepsilon_0}\int_a^b\frac{q}{r^2}dr
    = \left. \frac{1}{4\pi\varepsilon_0}\frac{q}{r^2} \right|_a^b
    = \frac{1}{4\pi\varepsilon_0}(\frac{q}{r_a}-\frac{q}{r_b})
\end{equation}
It follows that the closed path integral must equal 0,
and applying Stoke's theorem we get
\begin{equation}\label{rot_E}
    \oint_\Gamma \vec{E}d\vec{l}
    =\iint_S (\vec{\nabla}\times\vec{E})\cdot d\vec{a}
    = 0
    \rightarrow \vec{\nabla}\times\vec{E}
    = 0
\end{equation}


\subsection{Electric potential}
\label{Electric potential}

\subsubsection{The porential function}
\label{The porential function}
From analyse III we know that if the curl of a vector field is 0,
then the field derives a potential.
\begin{equation}\label{potential_of_E}
    \vec{E} = -\vec{\nabla}\phi
\end{equation}
\begin{equation}\label{poisson_equation}
    \nabla^2\phi = -\frac{\rho}{\varepsilon_0}
\end{equation}
meaning that the potential difference is given by
\begin{equation}\label{difference_in_potential}
    \Delta\phi
    = \phi(\vec{r_1})-\phi\vec{r_0}
    = -\int_{P_0}^{P_1}\vec{E}\,d\vec{l}
\end{equation}
The potential is commonly defined using the hypothesis that there is no charge at infinity.
\begin{equation}\label{potential_function}
    \phi(\vec{r}) = - \int_\infty^r \vec{E}\,d\vec{l}
\end{equation}
Luckily, the potential like most everything electronics allows for the superposition principle.
Here over a continuous charge distribution.
\begin{equation}\label{potential_from_charge_distribution}
    \phi(\vec{r})
    = \frac{1}{4\pi\varepsilon_0}
    \int
    \frac  {\rho(\vec{r})}
    {\|\vec{r}-\vec{r'}\|}
    \,d\tau
\end{equation}



\subsubsection{Work in an electric field}
\label{Work in an electric field}
Work as defined in general physics I is
\begin{equation}\label{work_phys_I}
    \int_a^b \vec{F} \cdot d\vec{l}
\end{equation}
And thus by applying equations \eqref{F=qE} and \eqref{potential_function} we get
\begin{equation}\label{potential_and_work}
    W = -Q \int_a^b\vec{E} \,d\vec{l}
    = Q \left(
    \Delta\phi(\vec{b})
    - \Delta\phi(\vec{a})
    \right)
\end{equation}


\subsection{Ideal conductors}
\label{Ideal conductors}
In an ideal conductor, electrons (charges) move freely, allowing for
\begin{equation}\label{E_field_inside_conductor}
    \vec{E}_{inside} = 0
\end{equation}
\begin{equation}\label{charge_distribution_inside_conductor}
    \rho_{inside} = 0
\end{equation}

meaning that the net charge is situated on the surface of the conductor and the $\vec{E}$ field outside the conductor is perpendicular to the surface.

\subsubsection{Induced charges}
\label{Induced charges}
Because the $\vec{E}$ field inside a conductor must equal 0,
there must be charges on the surface of the conductor if an external $\vec{E}$ field is present.
This leads to an $\vec{E}$ field perpendicular to the surface
\begin{equation}\label{E_field_on_surface_of_conductor}
    \vec{E}(\vec{x}) = \frac{\rho(\vec{x})}{\varepsilon_0}\hat{n }
\end{equation}
Force pushing charges outside is
\begin{equation}\label{force_on_charges_in_conductor_(static)}
    \vec{F} = \frac{\rho^2}{2\varepsilon_0}\hat{n}
\end{equation}
and reforming the equation using
eq \eqref{force_on_charges_in_conductor_(static)}
we get the ``pressure'' on charges
\begin{equation}\label{Pressure_on_charges_in_conductor}
    P = \frac{\varepsilon_0}{2}E^2
\end{equation}


