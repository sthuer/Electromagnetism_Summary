\section{Capacitors and dielectrics}
\subsection{Capacitors}\label{Capacitors}
Assuming there are two conductors with potentials $\phi_+$ and $\phi_-$,
\begin{equation}\label{delta_v_between_conductors}
    \Delta\phi = \phi_+ -\phi_- = \int_-^+ \vec{E} \,d\vec{l}
\end{equation}
which can be difficult so solve,
but we know $\vec{E} \propto Q$ hence the constant of proportionality is the capacitance
\begin{equation}\label{C=Q/V}
    C\equiv \frac{Q}{\Delta\phi}
\end{equation}

\subsubsection{Plate capacitor}
\label{Plate capacitor}
The potential difference is given by
\begin{equation}\label{voltage_difference_two_plate_conductors}
    \Delta\phi
    =2\int_0^d \frac{\rho}{2\varepsilon_0} \,dx
    = \frac{\rho}{\varepsilon_0}d
    = \frac{Q}{\varepsilon_0A}d
\end{equation}
which leads to a capacitance of
\begin{equation}\label{Capacitance_of_plate_capacitor}
    C = \frac{QA\varepsilon_0}{Qd} = \frac{A\varepsilon_0}{d}
\end{equation}
Note also the $E$ field inside the capacitor as constant.
\begin{equation}\label{E_field_in_plate_capacitor}
    E = \frac{1}{\varepsilon_0}\frac{Q}{A}
\end{equation}

\subsubsection{Energy of a capacitor}
\label{Energy of a capacitor}
To charge up a capacitor,
charges have to be moved against an $\vec{E}$ field.
\begin{equation}\label{Energy_in_capacitor}
    W = \int_0^QdW
    = \int_0^Q \frac{q}{C} \,dq
    = \frac{1}{2}\frac{Q^2}{C}
\end{equation}
\begin{equation}\label{W=1/2 CU^2}
    W = \frac{1}{2}C \left(\Delta\phi\right)^2
\end{equation}

\subsection{Dielectrics}
\label{Dielectrics}
\subsubsection{Induced dipoles}
\label{Induced dipoles}
Atoms subject to an $\vec{E}$ field find that,
the positive nucleus and the negative electrons are pulled apart even though as a whole the atom is neutral.
The induce dipole moment
\begin{equation}\label{dipole_moment}
    \vec{p} = \alpha\vec{E}
\end{equation}
is proportional to $\vec{E}$ where the proportionality constant $\alpha$ is the ``atomic polarizability''.
Note that,
$\vec{p}$ is in the same direction as $\vec{E}$,
meaning the vector points from the negative to the positive charge
(this is the opposite of how $\vec{E}$ was defined).

\subsubsection{Alignment of permanent dipoles}
\label{Alignment of permanent dipoles}
Some molecules like water have permanent dipole moments,
which will experience Torque when in an $\vec{E}$ field.
\begin{equation}\label{torque_of_dipole}
    \vec{\tau}
    = \vec{r}_+\times\vec{F}_+ + \vec{r}_-\times\vec{F}_-
    = q\vec{d}\times\vec{E}
\end{equation}
\begin{equation}\label{torque_of_dipole_moment}
    \vec{\tau} = \vec{p}\times\vec{E}
\end{equation}
And the potential energy of a dipole is described by
\begin{equation}
    U_{pot} = -\vec{p}\cdot\vec{E}
\end{equation}

\subsubsection{Dipole in an inhomogeneous field}
\label{Dipole in an inhomogeneous field}
If the $\vec{E}$ field is not homogeneous,
then a net force is applied to the dipole.
\begin{equation}\label{force_on_dipole}
    \vec{F} = q(\Delta\vec{E})
\end{equation}
where $\Delta\vec{E}$ is the difference between the plus and minus end of the dipole.
Assuming the distance $d$ is infinitesimal
\begin{equation}\label{force_on_dipole_condensed}
    \begin{split}
        \Delta\vec{E}&=(\vec{d}\cdot\nabla)\vec{E}
        \\
        \vec{F}&=(\vec{p}\cdot\nabla)\vec{E}
    \end{split}
\end{equation}
And so the torque in a non-uniform field is approximated by
\begin{equation}\label{torque_on_diple_inhomogen}
    \vec{\tau} = (\vec{p}\times\vec{E}) + (\vec{r}\times\vec{F})
\end{equation}
Note that the potential function of a dipole is given by
\begin{equation}\label{potential_of_dipole}
    \phi(\vec{r}) = \frac{\vec{p}\cdot\vec{r}}{4\pi\varepsilon_0r^3}
\end{equation}
Which allows us to find the electric field using the gradient as described in eq \eqref{potential_of_E}.
\begin{equation}\label{E_field_of_dipole}
    \vec{E}(r,\theta) = \frac{p}{4\pi\varepsilon_0r^3}
    (2\cos\theta \hat{r} + \sin\theta \hat{\theta})
\end{equation}

\subsubsection{Bound charges}
\label{Bound charges}
Assuming a piece of material having a dipole moment per unit volume $\vec{P}$ is given,
the potential function of the material is the sum over all dipoles.
\begin{equation}\label{potential_of_dipole_material}
    \phi(\vec{r})  =  \frac{1}{4\pi\varepsilon_0}
    \iiint_V \frac{\vec{P}(\vec{r}')\cdot(\vec{r}-\vec{r}')}
    {\left\lVert \vec{r}-\vec{r}' \right\rVert }
    \,dV
\end{equation}


Developing this integral and using the divergence theorem this integral starts to resemble the surface charges and volume charges.
\begin{equation}\label{potential_of_dielectric}
    \phi = \frac{1}{4\pi\varepsilon_0}
    \oiint_S \frac{1}{\left\lVert \vec{r}-\vec{r}'\right\rVert }\vec{P}\,d\vec{a}
    -
    \frac{1}{4\pi\varepsilon_0}
    \iiint_V \frac{1}{\left\lVert \vec{r}-\vec{r}'\right\rVert }
    (\nabla'\cdot \vec{P})\,dV
\end{equation}
and thus by using the surface charge
\begin{equation}\label{surface_charge}
    \sigma_b =\vec{P}\cdot\hat{n}
\end{equation}
and volume charge
\begin{equation}\label{volume_charge}
    \rho_b = - \nabla\cdot\vec{P}
\end{equation}
We get the following equation.
\begin{equation}\label{potential_of_dielectric_simplified}
    \phi(\vec{r}) = \frac{1}{4\pi\varepsilon_0}
    \oiint_S \frac{\sigma_b}{\left\lVert \vec{r}-\vec{r}'\right\rVert } \,d\vec{a}
    -
    \frac{1}{4\pi\varepsilon_0}
    \iiint_V \frac{\rho_b}{\left\lVert \vec{r}-\vec{r}'\right\rVert } \,dV
\end{equation}

\subsection{Electric displacement}
\label{Electric displacement}
The $\vec{E}$ field of a polarized medium is only due to the bound charges $\rho_b = -\nabla\cdot\vec{P}$, and the free charges $\rho_f$ (not bound to dipoles).
Using Gauss's law this gives us the total field
\begin{equation}\label{Gauss's_law_with_dipoles}
    \varepsilon_0\nabla\cdot\vec{E} = \rho = \rho_b + \rho_f = -\nabla\cdot\vec{P} + \rho_f
\end{equation}
So by defining the displacement $\vec{D}$ as
\begin{equation}\label{displacement_D}
    \vec{D} = \varepsilon_0\vec{E} + \vec{P}
\end{equation}
we get
\begin{equation}\label{displacement_free_charges}
    \begin{split}
        \nabla\cdot\vec{D} &= \rho_f
        \\
        \oiint_S\vec{D}\cdot\,d\vec{a} &= Q_{enc}
    \end{split}
\end{equation}

\subsection{Linear dielectrics}
Looking at the cause of $\vec{P}$ which is often (not always) proportional to $\vec{E}$ as long as it isn't too strong.
\begin{equation}\label{linear_dielectric}
    \vec{P} = \varepsilon_0\chi_e\vec{E}
\end{equation}
$\chi_e$ is the electric susceptibility.
Note that $\vec{E}$ is the \emph{total} field, which may be due to free charges as well as its own polarization.
Hence it is easiest to start with the electric displacement
\begin{equation}\label{displacement_vector}
    \begin{split}
        \vec{D} &= \varepsilon_0\vec{E} + \vec{P}
        \\
        &= \varepsilon_0\vec{E} + \varepsilon_0\chi\vec{E}
        \\
        &= \varepsilon_0\varepsilon_r\vec{E}
    \end{split}
\end{equation}
When deriving the $\vec{E}$ field inside a, we assume a plate capacitor and know that
$\langle E^{pol}\rangle = \frac{P}{\varepsilon_0}$
is the $E$ field inside a plate capacitor.
\begin{equation}\label{E_field_inside_dielectric}
    \langle E \rangle  = E^{ext} - \langle E^{pol}\rangle = E^{ext}-\frac{P}{\varepsilon_0} = \frac{E^{ext}}{\varepsilon_r}
\end{equation}

\subsubsection{Boundry conditions}
\label{Boundry conditions}
By using the gaussian surface over the border of two dielectrics
\begin{equation}\label{free_charges_between_dielectrics_gauss}
    \oiint_S \vec{D}\,d\vec{a} = (D_1 - D_2) \cdot S = \iiint_V\nabla\cdot\vec{D}dV = \iiint_V \rho_{free}\,dV = Q_{free}
\end{equation}
and thus
\begin{equation}\label{free_charges_between_dielectrics}
    D_1 - D_2 = \rho_{free}
\end{equation}

\subsubsection{Capacitance with dielectrics}
\label{Capacitance with dielectrics}
Inserting a dielectric into a capacitor increases the capacitance
% WARN is this always the case or just plate capacitors?
\begin{equation}\label{capacitance_with_dielectric}
    C_{\text{dielectric}} = \varepsilon_rC_{\text{vacuum}}
\end{equation}
and the bound charges amount to
\begin{equation}\label{capacitance_bound_charges_with_dielectric}
    Q_p = Q_{free} (1-\frac{1}{\varepsilon_r})
\end{equation}
