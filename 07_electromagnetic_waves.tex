\section{Electromagnetic waves}
\subsection{General waves}
A wave is a disturbance of a continuous medium that propagates with fixed shape at a constant velocity.
However, we will limit ourselves to sinusoidal waves.
\begin{equation}
    \label{wave_eq}
    \begin{split}
        \xi (z,t) &= A\cos{\left( k (z-vt) + \delta \right)}
        \\
        \xi (z,t) &= A\cos{\left (kz - \omega t + \delta \right)}
    \end{split}
\end{equation}
where $k$ is the wave number that characterizes the wavelength.
\begin{equation}\label{wave_num}
    \lambda = \frac{2\pi}{k}
\end{equation}
Combined with the propagation speed $v$,
k also defines the period $T$ and frequency $f$
\begin{equation}
    T = \frac{2\pi}{kv}
\end{equation}
\begin{equation}
    f = \frac{1}{T} = \frac{kv}{2\pi} = \frac{v}{\lambda}
\end{equation}

Equation \eqref{wave_eq} is basically the solution to the following differential equation that describes waves:
\begin{equation}
    \frac{\partial^2\xi}{\partial t^2} = v^2 \frac{\partial^2\xi}{\partial z^2}
\end{equation}
where $v$ is the wave velocity.
For a string it is defined by the density $\sigma$ and tension $T$.
\begin{equation}
    v^2 = \frac{T}{\sigma}
\end{equation}


\subsubsection{additional :: Boundry conditions transmission and reflections}
\label{additional :: Boundry conditions transmission and reflections}
Probably not necessary but since we saw it in EE-200 Electromagnétisme I : lignes et ondes might as well include it.
\begin{equation}\label{reflected_waves}
    \tilde{\xi}(z,t) = \left\{
    \begin{aligned}
         & \tilde{A}_Ie^{i\left( k_1z-\omega t \right)}
        + \tilde{A}_Re^{i\left( - k_1z-\omega t \right)}
         & \text{before incidence ($z<0$)}
        \\
         & \tilde{A}_Te^{i\left( k_2z - \omega t \right)}
         & \text{after incidence ($z>0$)}
    \end{aligned}
    \right.
\end{equation}
\begin{equation}
    \tilde{A}_I + \tilde{A}_R = \tilde{A}_T
\end{equation}
\begin{equation} \label{reflection_coefs}
    \begin{aligned}
        \tilde{A}_R & =\tilde{A}_I \left( \frac{k_1-k_2}{k_1+k_2} \right) & = \tilde{A}_I \left( \frac{v_2-v_1}{v_2+v_1} \right) \\
        \tilde{A}_T & = \tilde{A}_I \left( \frac{2k_1}{k_1+k_2} \right)   & = \tilde{A}_I\left( \frac{2v_2}{v_1+v_2} \right)     \\
    \end{aligned}
\end{equation}


\subsection{Electromagnetic waves in vacuum}
\label{Electromagnetic waves in vacuum}
In an electromagnetic wave, $\vec{E}$ and $\vec{B}$ are perpendicular to one another.
\begin{equation}\label{electromagnetic_wave}
    \begin{split}
        \vec{E}(z,t) &= E_0 \cos{\left(kz - \omega t\right)} \hat{n}\\
        \vec{B}(z,t) &= \frac{E_0}{c} \cos{\left(kz - \omega t\right)} (\hat{k} \times \hat{n})
    \end{split}
\end{equation}


\subsubsection{Poynting vector, energy, and momentum}
\label{Poynting vector, energy, and momentum}
The energy stored in electric/magnetic fields per unit volume is
\begin{equation}\label{energy_of_el/mag_fields}
    u_{EM} = \frac{1}{2}\left( \varepsilon_0E^2 + \frac{1}{\mu_0}B^2 \right)
\end{equation}
And in monochromatic wave, B can be written in terms of E.
\begin{equation}\label{energy_of_el_wave}
    u_{EM} = \varepsilon_0 E^2 = \varepsilon_0 E_0^2 \cos^2{\left( kz - \omega t \right)}
\end{equation}
As the wave travels, it carries this energy along with it and it is characterized by the poynting vector $\vec{S}$.
\begin{equation} \label{poynting_vector}
    \vec{S}
    = \frac{1}{\mu_0}\left(\vec{E}\times\vec{B}\right)
    = \vec{E}\times\vec{H}
    = cu_{EM}\,\hat{z}
\end{equation}
$\vec{S}$ is the energy per unit area per unit time.
The power exerted by the wave is
\begin{equation}
    P = \frac{dW}{dt} = -\frac{d}{dt}\iiint_V u\,dV - \oiint_S\vec{S}\,d\vec{a}
\end{equation}
And because there are no charges in vacuum,
$\frac{dW}{dt} = 0$ which means that
\begin{equation}
    \begin{split}
        - \iiint_V \frac{du}{dt}\,dV &= \oiint_V\vec{S}\,d\vec{a}\\
        -\frac{\partial u}{\partial t} &= \nabla \cdot \vec{S}
    \end{split}
\end{equation}


\subsubsection{Intensity and momentum}
The momentum of an electromagnetic wave is described as follows:
\begin{equation}
    \vec{p} = \frac{1}{c^2}\vec{S}
\end{equation}
Because light is super fast,
we normally don't care about the oscillations of the waves but prefer inspecting the time average.
The time average of a T-periodic function $f(t)$ is defined as
\begin{equation}
    \left\langle f \right\rangle = \frac{1}{T}\int_0^Tf(t)\,dt
\end{equation}
which leads us to the time averaged properties of an electromagnetic wave.
\begin{equation}
    \left\langle u_{EM} \right\rangle  = \frac{1}{2}\varepsilon_0E_0^2
\end{equation}
\begin{equation}
    \left\langle \vec{S} \right\rangle = \frac{1}{2}c\varepsilon_0E_0^2\,\hat{z}
\end{equation}
\begin{equation}
    \left\langle \vec{p} \right\rangle = \frac{1}{2c}\varepsilon_0E_0^2\,\hat{z}
\end{equation}
Notably, the magnitude of the time averaged poynting vector is called the electromagnetic intensity.
\begin{equation}\label{em_intensity}
    I = \left\langle S \right\rangle = \frac{1}{2} c \varepsilon_0 E_0^2
\end{equation}

And finally, because waves have momentum,
they exert pressure.
(Radiation pressure is the force per area.)
\begin{equation}
    \text{Radiation pressure } \mathfrak{P}= \frac{\left\langle S \right\rangle }{c} = \frac{I}{c}
\end{equation}
For a perfect absorber,
the above equation is derived by examining the momentum.
\begin{equation}
    \left\langle \left\lvert \vec{p}\right\rvert \right\rangle = \left\langle u_{EM}\right\rangle = \frac{I}{c^2}
\end{equation}
\begin{equation}
    \Delta p = \left\langle \left\lvert \vec{p}\right\rvert \right\rangle Ac\Delta t
\end{equation}
\begin{equation}
    \mathfrak{P} = \frac{1}{A}\frac{\Delta p}{\Delta t} = \frac{I}{c} = \frac{1}{2}\varepsilon_0 E_0^2
\end{equation}


\subsection{Photon}
\begin{equation}
    \text{Energy } E = hf = \hslash \omega
\end{equation}
\begin{equation}
    \text{Momentum} p = \frac{h}{\lambda} = \frac{E}{c} = \frac{hf}{c}
\end{equation}


\subsection{Emission of electromagnetic waves}
A dipole antenna emits EM waves radially outward,
but not along its axis.
The amplitude of the dipole is
\begin{equation}
    \begin{split}
        \Pi_0 &= Ql = \frac{I_0}{\omega}l \\
        \Pi &= Ql\sin{\left( \omega t \right)} \,\hat{z}
    \end{split}
\end{equation}
The magnitude of the $\vec{E}$ field of a wave originating from a dipole (given in polar spherical) is
\begin{equation}
    \left\lvert \vec{E}(r,t) \right\rvert
    = \frac{\Pi_0\sin{\theta}}{4\pi\varepsilon_0r}\frac{\omega^2}{c}\cos{\left( kr - \omega t \right)}
\end{equation}
And by using eq \eqref{em_intensity} we can calculate the intensity to be
\begin{equation}
    I(r,\theta) = \frac{\Pi_0^2\omega^4}{32\pi^2c^3\varepsilon_0r^2} \sin^2{\theta}
\end{equation}
And the total radiated power $\left\langle P \right\rangle$ can be calculated using the poynting vector.
\begin{equation}
    \begin{split}
        \left\langle P \right\rangle
        &= \oiint_{sphere} \left\langle \vec{S} \right\rangle \,d\vec{a} \\
        &= \frac{\Pi_0^2\omega^4}{12\pi\varepsilon_0c^3} \\
        &= \frac{I_0^2\omega^2l^2}{12\pi\varepsilon_0c^3}
    \end{split}
\end{equation}
In the lecture notes, the final equation is not divided by $\pi$,
which I assume is an error.
In any case, it is entirely possible that I have introduced an error and the last equation should be scrutinized before blindly using.


\subsection{Superposition principle}
\label{Superposition principle}
If there is a counterpropagating wave,
then it comes to interference and the two waves are added together.
This can result in a standing wave:
\begin{equation}
    \begin{split}
        \vec{E} &= 2E_0\left[ \sin{\left(ky\right)} \cos{\left(\omega t\right)} \right] \,\hat{z} \\
        \vec{B} &= -2\frac{E_0}{c}\left[ \cos{\left(ky\right)} \sin{\left(\omega t\right)} \right] \,\hat{x}
    \end{split}
\end{equation}
